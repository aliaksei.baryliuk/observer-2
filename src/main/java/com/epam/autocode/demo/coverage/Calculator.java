package com.epam.autocode.demo.coverage;

import java.util.List;

public class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    public String sayHello(List<String> stringList){
        long count = stringList.stream()
                .flatMapToInt(String::chars)
                .map(Character::charCount)
                .count();
        System.out.println(count);

        return "Hello world";
    }

    public String sayHello2(List<String> stringList){
        long count = stringList.stream()
                .flatMapToInt(String::chars)
                .map(Character::charCount)
                .count();

        System.out.println(count);

        return "Hello world";
    }
}
